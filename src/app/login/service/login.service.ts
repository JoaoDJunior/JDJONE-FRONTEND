import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { tap, delay } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { LoginAuth } from '../model/login-auth';



@Injectable()
export class LoginService {

  public API = environment.apiUrl;
  public LOGIN = this.API + '/login';
  public LOGINS = this.API + '/logins';

  public isLoggedIn = false;
  public redirectUrl: string;
  constructor(/*public jwtHelper: JwtHelperService*/private http: HttpClient) { }

  /*public isAutenticado(): boolean {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }*/

  public login(email: string, senha: string, loginAuth: LoginAuth): Observable<boolean> {
    return of(true).pipe(delay(1000),tap(val => {
      this.http.get(this.LOGIN + '/' + email + '/' + senha).subscribe(login => {
        if(login['token']) {
          loginAuth = new LoginAuth(login['id'], login['email'], login['senha'], login['username'], login['token'], login['tipo']);
          this.isLoggedIn = true;
        } else {
          this.isLoggedIn = false;
        }
      });
      
    }));

  }

  public logout(): void {
    this.isLoggedIn = false;
  }

}
