import { TestBed, inject } from '@angular/core/testing';

import { AutloginguardService } from './autloginguard.service';

describe('AutloginguardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AutloginguardService]
    });
  });

  it('should be created', inject([AutloginguardService], (service: AutloginguardService) => {
    expect(service).toBeTruthy();
  }));
});
