import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { LoginService } from '../service/login.service';
import { NgForm } from '@angular/forms';
import { LoginAuth } from '../model/login-auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public message: string;

  public loginAuth = new LoginAuth(0, '', '', '', '', '')

  constructor(private router: Router, public auth: LoginService) {
    this.setMessage();
   }

  ngOnInit() {
  }

  login(form: NgForm) {
    console.log(form);
    
    const email = form['email'];
    const senha = form['senha'];
    if(email && senha) {
      this.auth.login(email, senha, this.loginAuth).subscribe(() => {
        this.setMessage();
        if(this.auth.isLoggedIn) {
          console.log(this.loginAuth);
          let redirect = this.auth.redirectUrl ? this.auth.redirectUrl : '/painel';
          let navigationExtras: NavigationExtras = {
            queryParamsHandling: 'preserve',
            preserveFragment: true
          };

          this.router.navigate([redirect], navigationExtras);
        }
      });
    }
  }

  logout() {
    this.auth.logout();
  }
  setMessage() {
    this.message = 'Logged' + (this.auth.isLoggedIn ? 'in' : 'out');
  }

}
