export class LoginAuth {

    constructor(
        public id: Number,
        public email: string,
        public senha: string,
        public username: string,
        public token: string,
        public tipo: string
    ) {}
}
