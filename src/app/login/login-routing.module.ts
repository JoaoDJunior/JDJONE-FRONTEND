import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LoginService } from './service/login.service';
import { AuthloginguardService } from './service/authloginguard.service';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
]

@NgModule({
  exports:[RouterModule],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
  providers: [
    LoginService, AuthloginguardService
  ]
})
export class LoginRoutingModule { }
