import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatorioCenterComponent } from './relatorio-center.component';

describe('RelatorioCenterComponent', () => {
  let component: RelatorioCenterComponent;
  let fixture: ComponentFixture<RelatorioCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatorioCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatorioCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
