import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RelatorioCenterComponent } from './relatorio-center/relatorio-center.component';
import { RelatorioComponent } from './pages/relatorio/relatorio.component';

const relatorioRoutes: Routes = [
  { path:'', component: RelatorioCenterComponent, children: [
    { path:'', component: RelatorioComponent },
    //{ path:'representante', component: RepresentanteComponent },
  ] },
]

@NgModule({
  exports: [RouterModule],
  imports: [
    CommonModule,
    RouterModule.forChild(relatorioRoutes)
  ],
  declarations: []
})
export class RelatorioRoutingModule { }
