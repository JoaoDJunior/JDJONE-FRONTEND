import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { FileUploader } from 'ng2-file-upload';

const uri = environment.apiUrl + '/file/upload';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.css']
})
export class RelatorioComponent implements OnInit {

  uploader: FileUploader = new FileUploader({url:uri});

  constructor() { }

  ngOnInit() {
  }

}
