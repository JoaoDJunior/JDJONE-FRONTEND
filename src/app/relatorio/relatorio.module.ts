import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RelatorioComponent } from './pages/relatorio/relatorio.component';
import { RelatorioCenterComponent } from './relatorio-center/relatorio-center.component';
import { RelatorioService } from './service/relatorio.service';
import { RelatorioRoutingModule } from './relatorio-routing.module';

@NgModule({
  imports: [
    CommonModule,
    RelatorioRoutingModule,
  ],
  declarations: [
    RelatorioCenterComponent,
    RelatorioComponent
  ],
  providers: [
    RelatorioService
  ]
})
export class RelatorioModule { }
