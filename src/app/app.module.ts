import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import {FileUploadModule} from 'ng2-file-upload';

//Angular MaterialMatTableDataSource
import { MatButtonModule, MatCardModule, MatInputModule, MatListModule, MatToolbarModule, MatTableModule, MatTabsModule, MatPaginatorModule, MatTableDataSource  } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AppComponent } from './app.component';

import { HomeModule } from './home/home.module';

import { LoginComponent } from './login/login/login.component';
import { AppRoutingModule } from './/app-routing.module';
import { Error404Component } from './pages/error404/error404.component';
import { LoginRoutingModule } from './login/login-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    Error404Component,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    BrowserAnimationsModule,
    FileUploadModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatTableModule,
    MatTabsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    AppRoutingModule,
    LoginRoutingModule
    
  ],
  schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }
