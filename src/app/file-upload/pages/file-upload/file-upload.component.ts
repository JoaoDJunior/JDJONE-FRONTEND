import { Component, OnInit } from '@angular/core';
import { FileUploadService } from '../../service/file-upload.service';
import { FileUploader } from 'ng2-file-upload';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import {saveAs} from 'file-saver';
import { NgClass, NgStyle} from '@angular/common';
import { environment } from '../../../../environments/environment';

//const uri = 'http://localhost:8080/api/file/upload';
const uri = environment.apiUrl + '/file/upload';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
  providers: [FileUploadService]
})

//https://valor-software.com/ng2-file-upload/
export class FileUploadComponent implements OnInit {
  //Esta biblioteca recebe diretamente a url para fazer upload
  uploader: FileUploader = new FileUploader({url:uri});
  public hasBaseDropZoneOver:boolean = false;
  public hasAnotherDropZoneOver:boolean = false;

  attachmentList: any = [];

  selectedFiles: FileList;
  currentFileUpload: File;
  progress: { percentage: number } = { percentage: 0 };

  constructor(private fileService: FileUploadService) {
      this.uploader.onCompleteItem = (item:any, response:any , status:any, headers:any) => {}
    }

    ngOnInit() {}

    // Este método era usado junto com o file service, para um upload mais simples.
    upload() {
      this.progress.percentage = 0;
      
      this.currentFileUpload = this.selectedFiles.item(0)
      this.fileService.uploadFile(this.currentFileUpload).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress.percentage = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          console.log('File is completely uploaded!');
        }
      });
      this.selectedFiles = undefined;
    }
    
    selectFile(event) {
      this.selectedFiles = event.target.files;
    }
    public fileOverBase(e:any):void {
    console.log(e);
    
    this.hasBaseDropZoneOver = e;
  }
  
  public fileOverAnother(e:any):void {
    console.log(e);
    this.hasAnotherDropZoneOver = e;
  }
  
  download(index){
      var filename = this.attachmentList[index].uploadname;
  
      this.fileService.downloadFile(filename)
      .subscribe(
          data => saveAs(data, filename),
          error => console.error(error)
      );
  }
}
