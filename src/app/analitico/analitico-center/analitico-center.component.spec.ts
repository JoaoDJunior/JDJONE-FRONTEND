import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnaliticoCenterComponent } from './analitico-center.component';

describe('AnaliticoCenterComponent', () => {
  let component: AnaliticoCenterComponent;
  let fixture: ComponentFixture<AnaliticoCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnaliticoCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnaliticoCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
