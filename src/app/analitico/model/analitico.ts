import { Faturamento } from "./faturamento";
import { Empresa } from "../../empresa/model/empresa";

export class Analitico {
    constructor(
        public id: Number,
        public mes: Number,
        public ano: Number,
        public empresa: Empresa,
        public faturamento: Faturamento,
        public acao: string
    ) {}
}
