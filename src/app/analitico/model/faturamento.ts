export class Faturamento {

    constructor(
        public id: Number,
        public pl: Number,
        public hib: Number,
        public fatPl: Number,
        public fatHib: Number,
        public delta: Number) {}
}
