import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AnaliticoCenterComponent } from './analitico-center/analitico-center.component';
import { AnaliticoComponent } from './pages/analitico/analitico.component';
import { ViewAnaliticoComponent } from './pages/view-analitico/view-analitico.component';

const analiticoRoutes: Routes = [
  { path:'', component: AnaliticoCenterComponent, children: [
    { path:'', component: AnaliticoComponent },
    { path:'analitico', component: AnaliticoComponent },
    { path:'view-analitico/:id', component: ViewAnaliticoComponent },
    
  ] },
  
]

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(analiticoRoutes),
    
  ],
  declarations: []
})
export class AnaliticoRoutingModule { }
