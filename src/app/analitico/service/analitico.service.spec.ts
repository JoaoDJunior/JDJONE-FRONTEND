import { TestBed, inject } from '@angular/core/testing';

import { AnaliticoService } from './analitico.service';

describe('AnaliticoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnaliticoService]
    });
  });

  it('should be created', inject([AnaliticoService], (service: AnaliticoService) => {
    expect(service).toBeTruthy();
  }));
});
