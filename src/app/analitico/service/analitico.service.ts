import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import{ Observable } from 'rxjs/Observable';
import { FaturamentoService } from './faturamento.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class AnaliticoService {

  //public API = '/api';
  public API = environment.apiUrl;
  public ANALITICO_API = this.API + '/analitico';

  constructor(private http: HttpClient, faturamentoService: FaturamentoService) {}

  getAll(): Observable<any> {
      return this.http.get(this.ANALITICO_API);
  }

  getAnalitico(id: string) {
    return this.http.get(this.ANALITICO_API + '/' + id);
  }

  getEmpresa(id: string) {
    return this.http.get(this.ANALITICO_API + '/' + id + '/empresa');
  }

  getFaturamento(id: string) {
    return this.http.get(this.ANALITICO_API + '/' + id + '/faturamento');
  }

  save(analitico: any): Observable<any> {
    let result: Observable<Object>;
    if (analitico['href']) {
      result = this.http.put(analitico.href, analitico);
    } else {
      result = this.http.post(this.ANALITICO_API, analitico);
    }
    return result;
  }

  remove(id: string) {
    return this.http.delete(this.ANALITICO_API +'/'+ id);
  }

}
