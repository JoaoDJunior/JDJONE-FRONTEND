import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import{ Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class FaturamentoService {

  //public API = '/api';
  public API = environment.apiUrl;
  public FATURAMENTO_API = this.API + '/analitico';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.FATURAMENTO_API);
}

get(id: string) {
  return this.http.get(this.FATURAMENTO_API + '/' + id);
}

save(faturamento: any): Observable<any> {
  let result: Observable<Object>;
  if (faturamento['href']) {
    result = this.http.put(faturamento.href, faturamento);
  } else {
    result = this.http.post(this.FATURAMENTO_API, faturamento);
  }
  return result;
}

remove(id: string) {
  return this.http.delete(this.FATURAMENTO_API + '/' + id);
}

}
