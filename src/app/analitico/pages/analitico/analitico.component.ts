import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { RepresentanteService } from '../../../representante/service/representante.service';
import { Representante } from '../../../representante/pages/representante/representante.component';
import { tap } from 'rxjs/operators';
import { AnaliticoService } from '../../service/analitico.service';
import { Analitico } from '../../model/analitico';

@Component({
  selector: 'app-analitico',
  templateUrl: './analitico.component.html',
  styleUrls: ['./analitico.component.css']
})
export class AnaliticoComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  analiticos: Array<any>;
  displayedColumns = ['id','mes', 'ano', 'analitico.empresa.nomeFantasia', 'analitico.faturamento.id', 'acao'];
  dataSource: MatTableDataSource<Analitico>;

  constructor(private analiticoService: AnaliticoService) {}

  ngOnInit() {
    this.analiticoService.getAll().subscribe(data => {
      this.analiticos = data, error => console.error(error);
      console.log(this.analiticos);
      
      this.dataSource = new MatTableDataSource<Analitico>(this.analiticos);
      //this.empresas = Array.of(this.empresas);
      this.dataSource.paginator = this.paginator;
      for (const analitico of this.analiticos) {
        this.analiticoService.getEmpresa(analitico.id).subscribe(url => analitico.empresa = url);
      }
      for (const analitico of this.analiticos) {
        this.analiticoService.getFaturamento(analitico.id).subscribe(url => analitico.faturamento = url);
      }
    });
  }

  ngAfterViewInit() {
    this.paginator.page.pipe(tap(() => this.dataSource)).subscribe();
  }

  remove(id: string) {
    this.analiticoService.remove(id).subscribe(result => {
      window.location.reload()
    }, error => console.error(error));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}