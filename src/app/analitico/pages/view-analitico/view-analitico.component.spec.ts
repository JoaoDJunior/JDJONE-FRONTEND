import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAnaliticoComponent } from './view-analitico.component';

describe('ViewAnaliticoComponent', () => {
  let component: ViewAnaliticoComponent;
  let fixture: ComponentFixture<ViewAnaliticoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAnaliticoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAnaliticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
