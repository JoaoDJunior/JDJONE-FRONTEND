import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnaliticoRoutingModule } from './analitico-routing.module';
import { AnaliticoComponent } from './pages/analitico/analitico.component';
import { AnaliticoCenterComponent } from './analitico-center/analitico-center.component';
import { FormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MatTabsModule, MatTableModule, MatPaginatorModule, MatToolbarModule, MatListModule, MatInputModule, MatCardModule, MatButtonModule } from '@angular/material';
import { FileUploadModule } from 'ng2-file-upload';
import { HttpClientModule } from '@angular/common/http';
import { AnaliticoService } from './service/analitico.service';
import { FaturamentoService } from './service/faturamento.service';
import { ViewAnaliticoComponent } from './pages/view-analitico/view-analitico.component';

@NgModule({
  imports: [
    CommonModule,
    AnaliticoRoutingModule,
    HttpClientModule,
    FileUploadModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatTableModule,
    MatTabsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
  ],
  declarations: [
    AnaliticoComponent,
    AnaliticoCenterComponent,
    ViewAnaliticoComponent
  ],
  providers: [
    AnaliticoService, FaturamentoService
  ]
})
export class AnaliticoModule { }
