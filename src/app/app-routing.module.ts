import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login/login.component';
import { Error404Component } from './pages/error404/error404.component';
import { SelectivePreloadingStrategy } from './selective-preloading-strategy';
import {AuthloginguardService as AuthGuard} from './login/service/authloginguard.service';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'painel', loadChildren: 'app/painel/painel.module#PainelModule', data: { preload: true }, canLoad: [AuthGuard]  },
  { path: 'home', loadChildren: 'app/home/home.module#HomeModule', data: { preload: true }, canLoad: [AuthGuard]  },
  { path: 'file', loadChildren: 'app/file-upload/file-uploads.module#FileUploadsModule', data: { preload: true }, canLoad: [AuthGuard] },
  { path: 'relatorio', loadChildren: 'app/relatorio/relatorio.module#RelatorioModule', data: { preload: true }, canLoad: [AuthGuard] },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '**', component: Error404Component }
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes,
    {
      enableTracing: true, // <-- debugging purposes only
      preloadingStrategy: SelectivePreloadingStrategy,

    })],
    providers: [
      SelectivePreloadingStrategy
    ]
})

export class AppRoutingModule { }
