import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PainelcenterComponent } from './painel-center/painelcenter/painelcenter.component';
import { PainelComponent } from './pages/painel/painel.component';

const painelRoutes: Routes = [
  { path:'', component: PainelcenterComponent, children: [
    { path:'', component: PainelComponent },
    //{ path:'representante', component: RepresentanteComponent },
    
    
  ] },
  
]
@NgModule({
  exports: [RouterModule],
  imports: [
    CommonModule,
    RouterModule.forChild(painelRoutes)
  ],
  declarations: []
})
export class PainelRoutingModule { }
