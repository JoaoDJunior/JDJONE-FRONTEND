import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PainelcenterComponent } from './painelcenter.component';

describe('PainelcenterComponent', () => {
  let component: PainelcenterComponent;
  let fixture: ComponentFixture<PainelcenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PainelcenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PainelcenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
