import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-painelcenter',
  templateUrl: './painelcenter.component.html',
  styleUrls: ['./painelcenter.component.css']
})
export class PainelcenterComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

}
