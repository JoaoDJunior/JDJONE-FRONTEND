import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PainelcenterComponent } from './painel-center/painelcenter/painelcenter.component';
import { PainelComponent } from './pages/painel/painel.component';
import { PainelService } from './service/painel.service';
import { PainelRoutingModule } from './painel-routing.module';

@NgModule({
  imports: [
    PainelRoutingModule,
    CommonModule
  ],
  declarations: [
    PainelcenterComponent,
    PainelComponent
  ],
  providers: [PainelService]
})
export class PainelModule { }
