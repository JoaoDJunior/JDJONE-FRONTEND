import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login/service/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  message: string;

  title = 'JDJ-ONE';
  constructor(public router: Router, public auth: LoginService){
    
  }

  setMessage() {
    this.message = 'Logged' + (this.auth.isLoggedIn ? 'in' : 'out');
  }

  logout() {
    this.auth.logout();
  }

}
