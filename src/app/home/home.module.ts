import { NgModule } from '@angular/core';

import { HomeRoutingModule } from './home-routing.module';
import { HomeCenterComponent } from './home-center/home-center.component';

import { HttpClientModule } from '@angular/common/http';
import { FileUploadModule } from 'ng2-file-upload';
import { MatButtonModule, MatCardModule, MatInputModule, MatListModule, MatToolbarModule, MatPaginatorModule, MatTableModule, MatTabsModule } from '@angular/material';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    HomeRoutingModule,

    HttpClientModule,
    FileUploadModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatTableModule,
    MatTabsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,

  ],
  declarations: [
    HomeCenterComponent,    
  ]
})
export class HomeModule { }
