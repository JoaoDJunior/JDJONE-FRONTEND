import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HomeCenterComponent } from './home-center/home-center.component';

const homeCenterRoutes: Routes = [
  { path:'', component: HomeCenterComponent, children: [
    {path: 'empresa', loadChildren: 'app/empresa/empresa.module#EmpresaModule', data: { preload: true }},
    {path: 'analitico', loadChildren: 'app/analitico/analitico.module#AnaliticoModule', data: { preload: true }},
    {path: 'representante', loadChildren: 'app/representante/representante.module#RepresentanteModule', data: { preload: true }},
  ] },
]

@NgModule({
  imports: [
    RouterModule.forChild(homeCenterRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class HomeRoutingModule { }
