import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepresentanteCenterComponent } from './representante-center.component';

describe('RepresentanteCenterComponent', () => {
  let component: RepresentanteCenterComponent;
  let fixture: ComponentFixture<RepresentanteCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepresentanteCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepresentanteCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
