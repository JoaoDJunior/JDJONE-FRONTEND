import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RepresentanteRoutingModule } from './representante-routing.module';
import { RepresentanteComponent } from './pages/representante/representante.component';
import { RepresentanteCenterComponent } from './representante-center/representante-center.component';
import { RepresentanteService } from './service/representante.service';
import { HttpClientModule } from '@angular/common/http';
import { FileUploadModule } from 'ng2-file-upload';
import { MatButtonModule, MatCardModule, MatInputModule, MatListModule, MatToolbarModule, MatPaginatorModule, MatTableModule, MatTabsModule } from '@angular/material';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule } from '@angular/forms';
import { AddEditRepresentanteComponent } from './pages/add-edit-representante/add-edit-representante.component';

@NgModule({
  imports: [
    CommonModule,
    RepresentanteRoutingModule,
    HttpClientModule,
    FileUploadModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatTableModule,
    MatTabsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
  ],
  declarations: [
    RepresentanteComponent,
    AddEditRepresentanteComponent,
    RepresentanteCenterComponent,
  ],
  providers: [
    RepresentanteService
  ]
})
export class RepresentanteModule { }
