import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RepresentanteCenterComponent } from './representante-center/representante-center.component';
import { RepresentanteComponent } from './pages/representante/representante.component';
import { AddEditRepresentanteComponent } from './pages/add-edit-representante/add-edit-representante.component';

const representanteRoutes: Routes = [
  { path:'', component: RepresentanteCenterComponent, children: [
    { path:'', component: RepresentanteComponent },
    { path:'representante', component: RepresentanteComponent },
    { path:'add-representante', component: AddEditRepresentanteComponent },
    { path:'edit-representante/:id', component: AddEditRepresentanteComponent },
    
  ] },
  
]

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(representanteRoutes)
  ],
  declarations: []
})
export class RepresentanteRoutingModule { }
