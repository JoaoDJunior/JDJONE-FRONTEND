import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditRepresentanteComponent } from './add-edit-representante.component';

describe('AddEditRepresentanteComponent', () => {
  let component: AddEditRepresentanteComponent;
  let fixture: ComponentFixture<AddEditRepresentanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditRepresentanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditRepresentanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
