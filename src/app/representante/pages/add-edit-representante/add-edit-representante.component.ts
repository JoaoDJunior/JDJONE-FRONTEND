import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { RepresentanteService } from '../../service/representante.service';
import { NgForm } from '@angular/forms';
import { Representante } from '../../model/representante';

@Component({
  selector: 'app-add-edit-representante',
  templateUrl: './add-edit-representante.component.html',
  styleUrls: ['./add-edit-representante.component.css']
})
export class AddEditRepresentanteComponent implements OnInit, OnDestroy {

  private sub: Subscription;

   representante = new Representante( 0, '', '', '');

  constructor(private route: ActivatedRoute,
              private router: Router,
              private representanteService: RepresentanteService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      if (params['id']) {
        this.buscarRepresentante(params['id']);
      }
    }); 
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  gotoList() {
    this.router.navigate(['home/representante']);
  }

  buscarRepresentante(representanteId) {
    
    const id = representanteId;
    this.representanteService.get(id).subscribe((representante: Representante) => {
      if (representante) {
        console.log(representante);
        this.representante = representante;
      } else {
        console.log(`empresa with id '${id}' not found, returning to list`);
        this.gotoList();
      }
    });
  }

  save(id: Number, form: NgForm) {
    console.log(form);
    if(id != null) {
      form.value['id'] = id;
    }
    this.representante = form.value;
    console.log(this.representante);
    this.representanteService.save(this.representante).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

  remove(representante) {
    console.log(representante);
    
    this.representanteService.remove(representante).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

}
