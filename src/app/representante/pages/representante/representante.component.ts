import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RepresentanteService } from '../../service/representante.service';

@Component({
  selector: 'app-representante',
  templateUrl: './representante.component.html',
  styleUrls: ['./representante.component.css']
})
export class RepresentanteComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  representantes: Array<any>;

  displayedColumns = ['id','grc', 'ga', 'regional', 'acao'];
  dataSource: MatTableDataSource<Representante>;

  constructor(private representanteService: RepresentanteService) {}

  ngOnInit() {
    this.representanteService.getAll().subscribe(data => {
      this.representantes = data, error => console.error(error);
      this.dataSource = new MatTableDataSource<Representante>(this.representantes);
      this.dataSource.paginator = this.paginator;
      for (const representante of this.representantes) {
        //this.giphyService.get(representante.nomeFantasia).subscribe(url => representante.giphyUrl = url);
      } 
    });
  }

  ngAfterViewInit() {
    this.paginator.page.pipe(tap(() => this.dataSource)).subscribe();
  }

  remove(representante: Array<any>): void {
    this.representanteService.remove(representante['id'])
      .subscribe( data => {
        this.representantes = this.representantes.filter(u => u !== representante);
      })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}


export interface Representante {
  grc: string;
  ga: string;
  regional: string;
  id: number;
  acao: string;
  empresas: Array<any>;
}