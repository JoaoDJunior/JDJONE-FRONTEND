export class Representante {
    constructor(
        public id: Number,
        public ga: string,
        public grc: string,
        public regional:string
    ) { }
}
