import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import{ Observable } from 'rxjs/Observable';
import { Representante } from '../model/representante';


@Injectable()
export class RepresentanteService {
  
  //public API = '/api';
  public API = 'https://jdjone-ws.herokuapp.com/api';
  public REPRESENTANTE_API = this.API + '/representante';

  constructor(private http: HttpClient) {}

  getAll(): Observable<any> {
      return this.http.get(this.REPRESENTANTE_API);
  }

  get(id: string) {
    return this.http.get(this.REPRESENTANTE_API + '/' + id);
  }

  save(representante: Representante): Observable<any> {
    let result: Observable<Object>;
    if (representante['id']) {
      result = this.http.put(this.REPRESENTANTE_API + '/' + representante['id'], representante);
    } else {
      result = this.http.post(this.REPRESENTANTE_API, representante);
    }
    return result;
  }

  remove(href: string) {
    return this.http.delete(href);
  }

}
