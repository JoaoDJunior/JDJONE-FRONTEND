import { TestBed, inject } from '@angular/core/testing';

import { AcessoService } from './acesso.service';

describe('AcessoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AcessoService]
    });
  });

  it('should be created', inject([AcessoService], (service: AcessoService) => {
    expect(service).toBeTruthy();
  }));
});
