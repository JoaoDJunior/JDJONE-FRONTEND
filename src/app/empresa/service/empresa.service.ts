import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import{ Observable } from 'rxjs/Observable';
import { Empresa } from '../model/empresa';
import { environment } from '../../../environments/environment';

@Injectable()
export class EmpresaService {

  //public API = '/api';
  public API = environment.apiUrl;

  public EMPRESA_API = this.API + '/empresa';

  constructor(private http: HttpClient) {}

  getAll(): Observable<any> {
      return this.http.get(this.EMPRESA_API);
  }

  get(id: string) {
    return this.http.get(this.EMPRESA_API + '/' + id);
  }

  save(empresa: Empresa): Observable<any> {
    let result: Observable<Object>;
    console.log(empresa);
    
    if (empresa['id']) {
      result = this.http.put(this.EMPRESA_API + '/' + empresa['id'], empresa);
    } else {
      result = this.http.post(this.EMPRESA_API, empresa);
    }
    console.log(result);
    
    return result;
  }

  remove(id: Number) {
    return this.http.delete(this.EMPRESA_API +'/'+ id);
  }

}
