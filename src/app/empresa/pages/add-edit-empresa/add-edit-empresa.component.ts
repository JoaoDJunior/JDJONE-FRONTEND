import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NgForm, FormControl } from '@angular/forms';
import { EmpresaService } from '../../service/empresa.service';
import { Empresa } from '../../model/empresa';
import { forEach } from '@angular/router/src/utils/collection';
import { Representante } from '../../../representante/model/representante';
import { RepresentanteService } from '../../../representante/service/representante.service';

@Component({
  selector: 'app-add-edit-empresa',
  templateUrl: './add-edit-empresa.component.html',
  styleUrls: ['./add-edit-empresa.component.css']
})
export class AddEditEmpresaComponent implements OnInit, OnDestroy {

  private sub: Subscription;

  empresa = new Empresa(0, '', '', '', '', '', '');

  representante = new Representante(0, '', '', '');

  representantes = Array<any>();

  constructor(private route: ActivatedRoute,
              private router: Router,
              private empresaService: EmpresaService,
              private representanteService: RepresentanteService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      console.log(params);
      if (params['id']) {
        this.buscarEmpresa(params['id']);
      }
    });
    this.buscarRepresentante();
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  gotoList() {
    this.router.navigate(['home/empresa']);
  }

  buscarEmpresa(empresaId) {
    
    const id = empresaId;
    this.empresaService.get(id).subscribe((empresa: Empresa) => {
      if (empresa) {
        console.log(empresa);
        this.empresa = empresa;
      } else {
        console.log(`empresa with id '${id}' not found, returning to list`);
        this.gotoList();
      }
    });
  }

  buscarRepresentante() {
    this.representanteService.getAll().subscribe((representantes: any) => {
      if(representantes) {
        this.representantes = representantes;
      }
    })
  }

  save(id: Number, form: NgForm) {
    console.log(form);
    if(id != null) {
      form.value['id'] = id;
    }

    this.empresa = form.value;

    console.log(this.empresa);
    this.empresaService.save(this.empresa).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

  remove(id: Number) {
    this.empresaService.remove(id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

}