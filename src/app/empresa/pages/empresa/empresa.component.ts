import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { Empresa } from '../../model/empresa';
import { EmpresaService } from '../../service/empresa.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  empresas: Array<any>;

  displayedColumns = ['cnpj','razaoSocial', 'nomeFantasia', 'rede', 'cidade', 'uf', 'acao'];
  dataSource: MatTableDataSource<Empresa>;
  constructor(private empresaService: EmpresaService) {}

  ngOnInit() {
    this.empresaService.getAll().subscribe(data => {
      this.empresas = data, error => console.error(error);
      this.dataSource = new MatTableDataSource<Empresa>(this.empresas);
      //this.empresas = Array.of(this.empresas);
      this.dataSource.paginator = this.paginator;
      for (const empresa of this.empresas) {
        //this.giphyService.get(empresa.nomeFantasia).subscribe(url => empresa.giphyUrl = url);
      } 
    });
  }

  ngAfterViewInit() {
    this.paginator.page.pipe(tap(() => this.dataSource)).subscribe();
  }

  remove(empresa: Array<any>): void {
    this.empresaService.remove(empresa['id'])
      .subscribe( data => {
        this.empresas = this.empresas.filter(u => u !== empresa);
      })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


}
