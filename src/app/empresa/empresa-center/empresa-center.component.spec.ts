import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpresaCenterComponent } from './empresa-center.component';

describe('EmpresaCenterComponent', () => {
  let component: EmpresaCenterComponent;
  let fixture: ComponentFixture<EmpresaCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresaCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresaCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
