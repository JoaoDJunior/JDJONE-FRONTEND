export class Empresa {

    constructor(
        public id: Number,
        public cnpj: string,
        public nomeFantasia: string,
        public razaoSocial: string,
        public rede: string,
        public cidade:string,
        public uf: string
    ){}
}
