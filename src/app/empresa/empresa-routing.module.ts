import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpresaComponent } from './pages/empresa/empresa.component';
import { Routes, RouterModule } from '@angular/router';
import { AddEditEmpresaComponent } from './pages/add-edit-empresa/add-edit-empresa.component';
import { EmpresaCenterComponent } from './empresa-center/empresa-center.component';

const empresaRoutes: Routes = [
  { path:'', component: EmpresaCenterComponent, children: [
    { path:'', component: EmpresaComponent },
    { path:'add-empresa', component: AddEditEmpresaComponent },
    { path:'edit-empresa/:id', component: AddEditEmpresaComponent },
  ] },
  
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(empresaRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class EmpresaRoutingModule { }
