import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpresaRoutingModule } from './empresa-routing.module';
import { AddEditEmpresaComponent } from './pages//add-edit-empresa/add-edit-empresa.component';
import { EmpresaComponent } from './pages/empresa/empresa.component';
import { FormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MatTabsModule, MatTableModule, MatPaginatorModule, MatToolbarModule, MatListModule, MatInputModule, MatCardModule, MatButtonModule } from '@angular/material';
import { FileUploadModule } from 'ng2-file-upload';
import { HttpClientModule } from '@angular/common/http';
import { EmpresaCenterComponent } from './empresa-center/empresa-center.component';
import { EmpresaService } from './service/empresa.service';
import { RepresentanteService } from '../representante/service/representante.service';

@NgModule({
  imports: [
    CommonModule,
    EmpresaRoutingModule,
    HttpClientModule,
    FileUploadModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatTableModule,
    MatTabsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
  ],
  declarations: [
    EmpresaComponent,
    AddEditEmpresaComponent,
    EmpresaCenterComponent
  ],
  providers: [
    EmpresaService, RepresentanteService
  ]
})
export class EmpresaModule { }
